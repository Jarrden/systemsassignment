#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <termios.h>  //_getch*/
#include <ctype.h>

#define MAXDATASIZE 100 /* max number of bytes we can get at once */

#define PORT_NO 12345 /* default PORT Number */

char userName[100];

int handleMainMenu();
char getch();
int checkForResponse(int numbytes, int sockfd, char *buf);
void hangman(int sockfd, char *response);
void Highscores(int sockfd, char *response);
int port = PORT_NO;
void sendString(int socket_id, char *string, size_t buffSize);



int main(int argc, char *argv[]) {
  // Setup things required for socket
	int sockfd, numbytes=0, i=0;
	char *buffer;
	size_t bufsize = 5;
  size_t characters;
	char buf[MAXDATASIZE];
	struct hostent *he;
	struct sockaddr_in their_addr; /* connector's address information */

  // Allocate the buffer
	buffer = (char *)malloc(bufsize * sizeof(char));
    if( buffer == NULL)
    {
        perror("Unable to allocate buffer");
        exit(1);
    }

  // Handle incorrect usage of client arguments
	if (argc == 1 && argc < 3) {
		fprintf(stderr,"usage: client_hostname port_number\n");
		exit(1);
	}


	if ((he=gethostbyname(argv[1])) == NULL) {  /* get the host info */
		herror("gethostbyname");
		exit(1);
	}

  // Set the port to argument provided
	if(argc == 3){
		port = atoi(argv[2]);
	}

  // Socket stuff
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}
	their_addr.sin_family = AF_INET;      /* host byte order */
	their_addr.sin_port = htons(port);    /* short, network byte order */
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8);     /* zero the rest of the struct */


  // On connection to the server
	if (connect(sockfd, (struct sockaddr *)&their_addr, \
	sizeof(struct sockaddr)) == -1) {
		perror("connect");
		exit(1);
	}


  // Tell the user that the server may be congested
  printf("Connecting to server. If this takes too long, server may be congested\n");

  // Recieving a response from the server means that a thread has been allocated
  // to handle the client's responses, continue with the game
  while(numbytes == 0){
    if ((numbytes=recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
      perror("recv");
      exit(1);
    }
  }

  // Display the Introduction
	printf("==========================================\n\n\n");
	printf("Welcome to the Online Hangman Gaming System\n\n\n");
	printf("==========================================\n\n\n");
	printf("You are required to logon with your registered Username and password\n\n");
	printf("Please enter your username-->");

  // Have the user input their username and send it to the server
	characters = getline(&buffer,&bufsize,stdin);
	sprintf(userName, "%s", buffer);
	strtok(userName, "\n");
	sendString(sockfd, buffer, characters);


  // Have the user input their password and send it to the server
	printf("Please enter your password-->");
	characters = getline(&buffer,&bufsize,stdin);
	sendString(sockfd, buffer, characters);

  // Check for a response from the server
  int checkResponse = 1;
	while(checkResponse == 1){
		int response = checkForResponse(numbytes, sockfd, buf);

    // If a response is recived, handle it
    // If 1, continue with the game
		if(response == 1){
			checkResponse = 0;

    // If 0, close application
		}else if(response == 0){
			printf("\nFailed to authenticate, closing application \n");
			exit(0);
		}
	}

	// Authentication complete, display menu;
	while(1){

    // Retrieve user input
		int menuResponse = handleMainMenu();
    char response[1];

    // Handle user input
		switch(menuResponse){
			case '1':
				printf("Hangman selected\n");

				response[0] = '1';
				hangman(sockfd, response);
				break;

			case '2':
        response[0] = '2';
				printf("Highscores selected\n");
        Highscores(sockfd, response);
				break;

			case '3':
        response[0] = '3';
				printf("Exiting application\n");
        sendString(sockfd, response, 1);
        exit(0);
				break;

		}
	}
}

// Send a string to the server
void sendString(int socket_id, char *string, size_t buffSize) {
   send(socket_id, string, sizeof(char) * buffSize, 0);
}


// Request and display Highscores
void Highscores(int sockfd, char *response){

  int numbytes=0;
	char buf[100];
  int counter = 0;

  // Request highscores from the server
	sendString(sockfd, response, 1);


  while(1){

    // Recieve response
    if ((numbytes=recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
      perror("recv");
      exit(1);
    }

    // If the server has not disconnected,
    if(numbytes != 0 ){

      // Display the response
			if(numbytes != 1 && buf[0] != -1){
				buf[numbytes] = '\0';
				printf("%s\n",buf);
				counter++;
			}

      // If the server replies saying no highscores, display that and return to menu
      if(buf[0] == -1){
          printf("NO HIGHSCORES YET\n");
          return;
        }

      // Server has no more highscores to display , return to the menu
      if (buf[0] == 0){
           return;
       }

       // If the server has dropped connection, exit client
    }else{
      printf("Lost Connection, Exiting\n");
      exit(0);
    }
  }
}


// handles the hangMan game logic
void hangman(int sockfd, char *response){

	int numbytes;
	char buf[100];
	int counter = -1;
	int playing = 1;

  // Tell the server the user has selected hangMan
  sendString(sockfd, response, 1);


	while(playing){

    // Recieve response from the server
		if ((numbytes=recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
			perror("recv");
			exit(1);
		}

    // If the server has not dropped connection,
		if(numbytes != 0){
      // Display what the server responds with, increment the counter
				if(numbytes != 1){

					buf[numbytes] = '\0';
					printf("%s",buf);
					counter++;
				}

    // Server has dropped connection, close the application
		}else{
      printf("Lost Connection, exiting\n");
      exit(0);
    }

    // When the counter is 3, the server expects a guess
		if(counter == 3){

      // Server tells the user if the game is still taking place
			if ((numbytes=recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
				perror("recv");
				exit(1);
			}

      // if the response is a 0, the client has lost, if 1 the client has won
      // If neither, continue playing
			if(numbytes != 0){
					if(buf[0] == 0){
						printf("\nBad luck %s! You have run out of guesses. The Hangman got you!\n", userName);
						playing = 0;
					}else if(buf[0] == 1){
						printf("\nCongratulations %s! You beat the HangMan!\n", userName);

						playing = 0;
					}
			}

      // Ask the user to guess a letter
			if(playing)
			printf("\nEnter Guess: ");
			while(playing){

				char guess[1];

        // Use getch to retrieve a user response without the need to press enter
				guess[0] = tolower(getch());


        // If the input is a letter, send it
				if(guess[0] >= 'a' && guess[0] <= 'z' || guess[0] >= 'A' && guess[0] <= 'Z'){
					sendString(sockfd, guess, 1);
					counter = -1;
					break;
				}
			}
		}
	}
}

// Displays the main menu to the client, and returns their response
int handleMainMenu(){

	printf("\n\nPlease enter a selection");
	printf("\n<1> Play Hangman");
	printf("\n<2> Show LeaderBoard");
	printf("\n<3> Quit");
	printf("\n\nSelect option 1-3 ->");

	while(1){

    // Use getch to retrieve user response without the need to press enter
		int response = getch();

    // Make sure the response is one of the menu options
		if(response - '0' >= 1 && response - '0' <= 3){
			return response;
		}
	}
}


// checks for a response from the server and returns the first character of buffer
int checkForResponse(int numbytes, int sockfd, char *buf){

  // Retrieve response from server
	if ((numbytes=recv(sockfd, buf, 100, 0)) == -1) {
		perror("recv");
		exit(1);
	}else{

    // If the server has not disconnected prematurely, return first character
		if(numbytes != 0){
			return buf[0] - '0';

    // else, close the client
		}else{
      printf("Lost Connection, exiting\n");
      exit(0);
    }
	}
}

// Function to cleanly get use input without pressing enter
char getch(){
    char buf=0;
    struct termios old={0};
    fflush(stdout);
    if(tcgetattr(0, &old)<0)
        perror("tcsetattr()");
    old.c_lflag&=~ICANON;
    old.c_lflag&=~ECHO;
    old.c_cc[VMIN]=1;
    old.c_cc[VTIME]=0;
    if(tcsetattr(0, TCSANOW, &old)<0)
        perror("tcsetattr ICANON");
    if(read(0,&buf,1)<0)
        perror("read()");
    old.c_lflag|=ICANON;
    old.c_lflag|=ECHO;
    if(tcsetattr(0, TCSADRAIN, &old)<0)
        perror ("tcsetattr ~ICANON");
    return buf;
 }
