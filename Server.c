#define _GNU_SOURCE
#include <arpa/inet.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <pthread.h>


#define DEFAULTPORT 12345
#define BACKLOG 10
#define NUM_THREADS 10 /*Maximum amount of threads*/

// For some reason, it doesn't like having NUM_THREADS
pthread_t p_threads[NUM_THREADS];

// synchronisation and thread handling
int read_count = 0;
pthread_mutex_t Read, Write;
pthread_mutex_t request_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;

// Global File opener
FILE* file;


/* global condition variable for our program. assignment initializes it. */
pthread_cond_t  got_request   = PTHREAD_COND_INITIALIZER;


/* format of a single request.*/
struct request {
    int new_fd;             /* number of the request                  */
    struct request* next;   /* pointer to next request, NULL if none. */
};


struct request* get_request(pthread_mutex_t* p_mutex);
struct request* requests = NULL;     /* head of linked list of requests. */
struct request* last_request = NULL; /* pointer to last request.         */


// Global Highscore variable and max threads
int highScores[25][2];
int num_requests = 0;   /* number of pending requests, initially none */


// Function  Declaration
void closeHandler(int sig);
int handleLogin(int new_fd);
int checkVictory(char *obscuredWord, char *charList);
void sendResponse(int new_fd, char response);
char *obfuscateWord(char *word, char *Letters, char *original);
int menuHandler(int new_fd, int userIndex);
int hangman(int new_fd, int userIndex);
void sendHMData(int new_fd, char *obf, char *letters, int gLeft);
void updateHighscore(int userIndex, int increment);
void displayHS(int new_fd);
void sendHS(int new_fd, char *userName, int wins, int games);
char *grabUserName(int index);
char *generateWords();
void* handle_requests_loop(void* data);
void add_request(int new_fd, pthread_mutex_t* p_mutex, pthread_cond_t*  p_cond_var);
void initiateHS();
void NewThread(struct request* a_request, int thread_id);


int main(int argc, char *argv[]){

  // Close signal handling
  signal(SIGINT, closeHandler);

  /* listen on sock_fd, new connection on new_fd */
  int sockfd, new_fd;

	// Initiate Highscores, set them to 0
	initiateHS();

	// Setup threads
	int thr_id[NUM_THREADS];
	//pthread_t p_threads[NUM_THREADS];

  // Put each thread in the handle_requests_loop
	for (size_t i = 0; i < NUM_THREADS; i++) {
		thr_id[i] = i;
		pthread_create(&p_threads[i], NULL, handle_requests_loop, (void*)&thr_id[i]);
	}

  // Initiate Mutex Locking for Reading/writing to Highscores
	if (pthread_mutex_init(&Read, NULL) != 0)
    {
        printf("\n mutex init failed\n");
        return 1;
    }
	if (pthread_mutex_init(&Write, NULL) != 0)
	  {
	    printf("\n mutex init failed\n");
	    return 1;
	  }


  // Socket Setup
	struct sockaddr_in my_addr;    /* my address information */
	struct sockaddr_in their_addr; /* connector's address information */
	socklen_t sin_size;
	int port;


	// If no port argument is supplied, set the port to the default
	if (argc != 2) {
		port = DEFAULTPORT;
		printf("Port bound to: %d\n", port);
	}else{

  	// If a port is supplied, set the port to that
  	if (argv[1] != NULL){

  		// Convert the parameter as an integer
  		port = atoi(argv[1]);

      if (port == 0){
        printf("Port bind unsuccessful, reverting to default. \n");
        port = DEFAULTPORT;
      }

        printf("Port bound to: %d\n", port);




  	}
  }

	/* generate the socket */
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}


	/* generate the end point */
	my_addr.sin_family = AF_INET;         /* host byte order */
	my_addr.sin_port = htons(port);     /* short, network byte order */
	my_addr.sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */

	if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) \
	== -1) {
		perror("bind");
		exit(1);
	}


	/* start listnening */
	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

	printf("server starts listnening ...\n");

	/* for every accepted connection, use a seperate process or thread to serve it */
	while(1) {  /* main accept() loop */

		sin_size = sizeof(struct sockaddr_in);
		if ((new_fd = accept(sockfd, (struct sockaddr *)&their_addr, \
		&sin_size)) == -1) {
			perror("accept");
			continue;
		}
		printf("server: got connection from %s\n", \
			inet_ntoa(their_addr.sin_addr));

      // Whenever a user connects to the server, add them to the request list
			add_request(new_fd, &request_mutex, &got_request);

	}
}


// InitiateHS sets the initial value of each highscore to 0
void initiateHS(){
	for (int i = 0; i < 10; i++) {

		for (int j = 0; j < 2; j++) {
			/* code */
			highScores[i][j] = 0;
		}
		/* code */
	}

}


// New Thread is called when a thread is assigned a client. This acts as the
// Beginning of the game loop
void NewThread(struct request* a_request, int thread_id){

  // Set the socket to the socket component of the request struct
	int new_fd = a_request->new_fd;

  // Tell the user that a server has been assigned to them;
	sendResponse(new_fd, 1+'0');

  // Setup Randomisation
	srand(time(NULL));

	// Handle user Authentication, returns 1 or 0, tells if user is logged in
	int loggedIn = handleLogin(new_fd);

  // Response is to be returned to user to tell them if they logged in
	char response;

  // Set response to whether the user has logged in or not
	if(loggedIn >= 1){
			response = 1+'0';
	}else{
			response = 0+'0';
	}


  // send response to client
	sendResponse(new_fd, response);

	// If the client has logged in successfully, continue
	if(loggedIn >= 1){
    // User menu option
		menuHandler(new_fd, loggedIn);

	}

  // Close connection and return thread to pool
  close(new_fd);
  return;

}


// adds connected clients to a request list
// Adapted from weekly tutorials
void add_request(int new_fd, pthread_mutex_t* p_mutex, pthread_cond_t*  p_cond_var)
{
    int rc;                         /* return code of pthreads functions.  */
    struct request* a_request;      /* pointer to newly added request.     */

    /* create structure with new request */
    a_request = (struct request*)malloc(sizeof(struct request));
    if (!a_request) { /* malloc failed?? */
        fprintf(stderr, "add_request: out of memory\n");
        exit(1);
    }
    a_request->new_fd = new_fd;
    a_request->next = NULL;

    /* lock the mutex, to assure exclusive access to the list */
    rc = pthread_mutex_lock(p_mutex);

    /* add new request to the end of the list, updating list */
    /* pointers as required */
    if (num_requests == 0) { /* special case - list is empty */
        requests = a_request;
        last_request = a_request;
    }
    else {
        last_request->next = a_request;
        last_request = a_request;
    }

    /* increase total number of pending requests by one. */
    num_requests++;

    /* unlock mutex */
    rc = pthread_mutex_unlock(p_mutex);

    /* signal the condition variable - there's a new request to handle */
    rc = pthread_cond_signal(p_cond_var);
}


// Where threads sit and wait for new requests to come in
// Adapted from weekly tutorials
void* handle_requests_loop(void* data)
{
    int rc;                         /* return code of pthreads functions.  */
    struct request* a_request;      /* pointer to a request.               */
    int thread_id = *((int*)data);  /* thread identifying number           */


    /* lock the mutex, to access the requests list exclusively. */
    rc = pthread_mutex_lock(&request_mutex);

    /* do forever.... */
    while (1) {

        if (num_requests > 0) { /* a request is pending */
            a_request = get_request(&request_mutex);
            if (a_request) { /* got a request - handle it and free it */
                /* unlock mutex - so other threads would be able to handle */
                /* other reqeusts waiting in the queue paralelly.          */
                rc = pthread_mutex_unlock(&request_mutex);
								//NewThread(int new_fd, int thread_id)
                NewThread(a_request, thread_id);
                free(a_request);
                /* and lock the mutex again. */
                rc = pthread_mutex_lock(&request_mutex);
            }
        }
        else {
            /* wait for a request to arrive. note the mutex will be */
            /* unlocked here, thus allowing other threads access to */
            /* requests list.                                       */

            rc = pthread_cond_wait(&got_request, &request_mutex);
            /* and after we return from pthread_cond_wait, the mutex  */
            /* is locked again, so we don't need to lock it ourselves */

        }
    }
}


// Gets the next request waiting for handling
// Adapted from weekly tutorials
struct request* get_request(pthread_mutex_t* p_mutex)
{
    int rc;                         /* return code of pthreads functions.  */
    struct request* a_request;      /* pointer to request.                 */

    /* lock the mutex, to assure exclusive access to the list */
    rc = pthread_mutex_lock(p_mutex);

    if (num_requests > 0) {
        a_request = requests;
        requests = a_request->next;
        if (requests == NULL) { /* this was the last request on the list */
            last_request = NULL;
        }
        /* decrease the total number of pending requests */
        num_requests--;
    }
    else { /* requests list is empty */
        a_request = NULL;
    }

    /* unlock mutex */
    rc = pthread_mutex_unlock(p_mutex);

    /* return the request to the caller. */
    return a_request;
}


// Handles the menu selections sent from the client
int menuHandler(int new_fd, int userIndex){

	char buf[100];
	int numbytes;
  int clientDisconnected = 0; // If a user has disconnected whilst playing a game

  // Forever...
	while(1){
		if ((numbytes=recv(new_fd, buf, 100, 0)) == -1) {
			perror("recv");
		}else{

			if(numbytes != 0){

        // Set the user selection to selection
        // the first array of the buffer is the user input
				int selection = (buf[0]);

        // Switch statement to handle input
				switch(selection){
          // Hangman
					case '1':
          // ClientDisconnected will be set to 1 if the user drops out whilst playing
					clientDisconnected = hangman(new_fd, userIndex);
					break;


          // Highscores
					case '2':

          // Display the Highscores
					displayHS(new_fd);
					break;


          // Quit game
					case '3':

          // Return to NewThread to close connection and return thread to pool
					return 1;
				}

        if(clientDisconnected){
          // Return to NewThread to close connection and return thread to pool
          return 1;
        }

        // If the user drops out in menu
			}else{
        // Return to NewThread to close connection and return thread to pool
        return 1;
      }
		}
	}
}


int hangman(int new_fd, int userIndex){

	char guessedLetters[28];
  int guessesLeft = 100;
	int charCount = 1;
	int numbytes;
	char buf[100];

  // Generate words
  char *word = generateWords();

  // Set each of the guessed letters to a space
	for (size_t i = 0; i < 28; i++) {
		/* code */
		guessedLetters[i] = ' ';
	}

  // Set the final character to be the null terminator needed for strings
	guessedLetters[27] = '\0';

  // Obfuscate the word and set it to a new string
  char *obfuscatedWord = (char *)malloc((strlen(word))* sizeof(char));
  strcpy(obfuscatedWord, word);

  // Take the guessed letters and add them to the obfuscated word
  obfuscateWord(obfuscatedWord, guessedLetters, word);

  // Determine the guesses from the size of the word
	if(strlen(word)+10-1 > 26){ // Minus 1 for the space in the word
		guessesLeft = 26;
	}else{
		guessesLeft = strlen(word)+10;
	}

  // send out game data
  sendHMData(new_fd, obfuscatedWord, guessedLetters, guessesLeft);
  // Free obfuscated word

  // Tell the user they are still playing
	sendResponse(new_fd, 2);

  int won;

  // hangman game loop
	int playing = 1;
  while(playing == 1){

		if(guessesLeft >= 1){
      won = 0;
			if ((numbytes=recv(new_fd, buf, 100, 0)) == -1) {
				perror("recv");
			}

      // If the user hasn't closed connection prematurely
			if(numbytes != 0){

        // Set the guess to the user's response
				char guess = (buf[0]);

        // Add the guess to the list
				guessedLetters[charCount] = guess;
				guessesLeft--;
				charCount++;

        // add guessed letters to obfuscated word where appropriate
				obfuscateWord(obfuscatedWord, guessedLetters, word);

        // Send the user the data
				sendHMData(new_fd, obfuscatedWord, guessedLetters, guessesLeft);


        // Check if player has won
				if(checkVictory(obfuscatedWord, word) == 1){
					//Client has beaten hangMan, tell user this and set playing to 0
					updateHighscore(userIndex, 1);
					sendResponse(new_fd, 1);
					playing = 0;
          won = 1;
				}


        // If the user has run out of guesses, update the highscore
        // and tell the user they have lost, exit out of the game loop
				if(guessesLeft == 0 && won == 0){

					updateHighscore(userIndex, 0);
					sendResponse(new_fd, 0);
					playing = 0;
				}else{
          sendResponse(new_fd, 2);
        }


				}else{
          // Client has disconnected, handle that
          free(obfuscatedWord);
          free(word);
          return 1;
        }
	   }
	}
  // The game finished without the user disconnecting
  free(obfuscatedWord);
  free(word);
  return 0;
}


// Update highscore updates the highscores, and prevents highscores from being
// read whils writing is occuring
void updateHighscore(int userIndex, int increment){

	// Lock mutex to prevent reading, and others writing
	pthread_mutex_lock(&Write);

  // 0 Refers to games won
  // 1 Refers to total games
  // Total index is the index of the user in Authentication.txt
	highScores[userIndex][0] += increment;
	highScores[userIndex][1]++;

  // Unlock mutex to allow for reading/writing
	pthread_mutex_unlock(&Write);

}

// Handles displaying the highscores
void displayHS(int new_fd){

	int numberOfScores = 0;

  // Readers writiers problem, adapted from lecture and textbook
  // Lock reading
	pthread_mutex_lock(&Read);
  // Increment the read_count
  // If the thread is the first to do so, lock mutex handling writing to highscores
  // and unlock reading
	read_count++;
	if(read_count == 1)
	pthread_mutex_lock(&Write);
	pthread_mutex_unlock(&Read);


  // Sorted array is an array of indexes referring to the highscore indexes
  // This holds the sorted order of the highscore indexes
  int sortedArray[25];

  // Set the initial values as unsorted
  for (size_t i = 0; i < 25; i++) {
    sortedArray[i] = i;
  }

  // High and low represent the higher and lower value when comparing two highscores
  // these are used to switch positions of the two scores when being compared
  int high = 0;
  int low = 0;


  // All sorting is done through the use of the bubble sort method

  // Sort by win count
  // While a score has been swapped
  int swapped = 1;
  while(swapped){
    // Set swapped to 0 to end loop if no swapping occurs this time
    swapped = 0;

    // Go through each highscore and compare two scores using the index of the
    // sorted array
    for (int i = 2; i < 25; i++) {

      // If the score -1 of i is smaller, switch the two score positions in
      // the sorted array
      if(highScores[sortedArray[i-1]][0] > highScores[sortedArray[i]][0]){
        high = sortedArray[i-1];
        low = sortedArray[i];
        sortedArray[i] = high;
        sortedArray[i-1] = low;

        // Set swapped to 1 as the sorting might not be finished
        swapped = 1;
      }
    }
  }


  // All sorting is done through the use of the bubble sort method

  // Sort by win average
  // While a score has been swapped
  swapped = 1;
  while(swapped){
    // Set swapped to 0 to end loop if no swapping occurs this time
    swapped = 0;

    // Go through each highscore and compare two scores using the index of the
    // sorted array
    for (int i = 2; i < 25; i++) {

      // If the score -1 of i is the same as the score of i and they are not 0
      if(highScores[sortedArray[i-1]][0] == highScores[sortedArray[i]][0] && highScores[sortedArray[i-1]][0] != 0){

        // If the average score -1 of i is smaller, switch the two score positions in
        // the sorted array
        if(highScores[sortedArray[i-1]][0]/highScores[sortedArray[i-1]][1] > highScores[sortedArray[i]][0]/highScores[sortedArray[i]][1]){
          high = sortedArray[i-1];
          low = sortedArray[i];
          sortedArray[i] = high;
          sortedArray[i-1] = low;

          // Set swapped to 1 as the sorting may not be finished
          swapped = 1;
        }
      }
    }
  }


  // All sorting is done through the use of the bubble sort method

  // Sort by Alphabetical order
  // While a score has been swapped
  swapped = 1;
  while(swapped){
    // Set swapped to 0 to end loop if no swapping occurs this time
    swapped = 0;

    // Go through each highscore and compare two scores using the index of the
    // sorted array
    for (int i = 2; i < 25; i++) {

      // If the score -1 of i is the same as the score of i and they are not 0
      if(highScores[sortedArray[i-1]][0] == highScores[sortedArray[i]][0] && highScores[sortedArray[i-1]][0] != 0){

        // If the average score -1 of i is the same as the score of i and they are not 0
        if(highScores[sortedArray[i-1]][0]/highScores[sortedArray[i-1]][1] == highScores[sortedArray[i]][0]/highScores[sortedArray[i]][1]){

          // If the the username of score i-1 is alphabetically higher than the i, switch them

          if(strncmp(grabUserName(sortedArray[i-1]),grabUserName(sortedArray[i]), 10) < 0){
            high = sortedArray[i-1];
            low = sortedArray[i];
            sortedArray[i] = high;
            sortedArray[i-1] = low;

            // Set swapped to 1 as the sorting may not be finished
            swapped = 1;
          }
        }
      }
    }
  }

  // Go through each of the highscores listed by the sorted array
  // and if the score has games played, send it to the user
	for (size_t i = 0; i < 25; i++) {
		if(highScores[sortedArray[i]][1] != 0){

      // increment the numberofscores
			numberOfScores++;
      sendHS(new_fd, grabUserName(sortedArray[i]), highScores[sortedArray[i]][0], highScores[sortedArray[i]][1]);
      }
		}

  // If the thread is the first thread to read, unlock writing to the highscore
	pthread_mutex_lock(&Read);
	read_count--;
	if(read_count == 0)
	pthread_mutex_unlock(&Write);
	pthread_mutex_unlock(&Read);

  // If number of scores is 0, tell the user there are no high scores
	if(numberOfScores == 0){
		sendResponse(new_fd, -1);
	}


	// When finished displaying the highscores, tell the user that there are no more
	sendResponse(new_fd, 0);
}

// Check victory Compares the two strings inputted
// Returns 1 if they are the same, 0 if they aren't
int checkVictory(char *obscuredWord, char *originalWord){
  if(strcmp(obscuredWord, originalWord)==0){
  	return 1;
  }else{
  	return 0;
  }
}


// Send highscores sends individual highscores to the user
void sendHS(int new_fd, char *userName, int gamesWon, int totalGames){

	char divider[50];
	char Name[50];
	char wins[50];
	char games[50];

  // Set the strings to their required values
	sprintf(divider, "\n===============================");
	sprintf(Name,"\nPlayer - %s", userName);
	sprintf(wins,"Number of games won - %d", gamesWon);
	sprintf(games,"Number of games played - %d", totalGames);


	// Send divider
	if (send(new_fd, divider, 100, 0) == -1)
  perror("send");

	// Send userName
	if (send(new_fd, Name, 100, 0) == -1)
  perror("send");

	// send wins
	if (send(new_fd, wins, 100, 0) == -1)
  perror("send");

	// send total games
	if (send(new_fd, games, 100, 0) == -1)
  perror("send");
	// Send divider
	if (send(new_fd, divider, 100, 0) == -1)
  perror("send");
}


// Grab userName Returns the userName from Authentication.txt
// using index as the line
char *grabUserName(int index){

  // Open the authentication file
	file = fopen("Authentication.txt", "r");
	char line[256];
	int counter = 0;

  // Go through each line
	while (fgets(line, sizeof(line), file)) {


    // If the counter is equal to index, retrieve the username and return it
		if(counter == index){

			char user[20], pass[20];
			sscanf(line, "%s %s", user, pass);

			char *userName = (char *)malloc((strlen(user))* sizeof(char));
			strcpy(userName, user);

      // Close file
			fclose(file);
      file = NULL;

			return userName;
		}
    // Increment line counter
		counter++;
	}

  // Close file
	fclose(file);
  file = NULL;
}


// Sends the required data for the user to play Hangman
void sendHMData(int new_fd, char *obf, char *letters, int gLeft){

  char GL[50];
  char Guess[50];
  char word[50];
	char divider[50];

  // Prepare the strings to be sent
	sprintf(divider, "\n\n===============\n");
  sprintf(GL, "\nGuessed Letters: %s\n", letters);
  sprintf(Guess, "\nNumber of guesses left: %d\n", gLeft);
  sprintf(word, "\nWord: %s\n", obf);


  // Send the divider
	if (send(new_fd, divider, 100, 0) == -1)
  perror("send");

  // Send the guessed letters
  if (send(new_fd, GL, 100, 0) == -1)
  perror("send");

  // Send the Number of guesses
  if (send(new_fd, Guess, 100, 0) == -1)
  perror("send");

  // Send the obfuscated word
  if (send(new_fd, word, 100, 0) == -1)
  perror("send");

}


// Obfuscates the word inputted, replacing all characters with '_'
// unless the letters guessed match a character in the word
char *obfuscateWord(char *theword, char *Letters, char *original){

  int n = 0;
  int i = 0;

  // Go through each of the words and each character with either a '_' or
  // a character from the guessed letter list
  while(n <= strlen(original)-1) {
    while(i <= strlen(Letters)) {

      if(original[n] == Letters[i]){
        i = 100;
        theword[n] = original[n];
      }else{
        theword[n] = '_';
      }
      i++;
    }
    n++;
    i = 0;
  }
}

char *generateWords(){

    // Read through each line and count them
		int linecount = 288;
    char line[256];

    // Open hangman text file
		file = fopen("hangman_text", "r");

    // Set r to a random number between 1 and the linecount
		int r = rand() % linecount;
		int counter = 0;

    // Go through each line
    while (fgets(line, sizeof(line), file)) {

      // If the line counter is equal to r, grab and tokenise the string on that
      // line
			if(counter == r){
	    	char object[20], type[20];
				char test;

        // Tokenise the type and the object into two seperate strings
	    	sscanf(line, "%[^,],%s", object, type);

        // Combine the two seperate strings into one in the correct order
	    	char *combined = (char *)malloc((strlen(object)+strlen(type))* sizeof(char));
	    	strcpy(combined, type);
				strcat(combined, " ");
	    	strcat(combined, object);

        // close the file
				fclose(file);
        file = NULL;

        // Return the combined string
				return combined;
			}
      // increment counter
      counter++;
    }
}

// Sends a single character to the client
void sendResponse(int new_fd, char response){
	send(new_fd, &response, sizeof(char), 0);

}


// Handles the user logging in, returns the index of the user if they have logged in
// Returns 0 if they have not logged in
int handleLogin(int new_fd){

	int numbytes = 0;
	char buf[100];

  // set every character in the buffer to nothing
	for (size_t i = 0; i < 100; i++) {
		buf[i] = ' ';
	}

	char userName[20];
	char password[20];
	char *combined;
	int loggingIn = 1;
	int loginStage = 0;


 	while(loggingIn == 1){

    // Recieve input from the user
		if ((numbytes=recv(new_fd, buf, 100, 0)) == -1) {
			perror("recv");
		}else{

      // If the user has not dropped out prematurely
			if(numbytes != 0){
				buf[numbytes-1] = '\0';

        // Loginstage of 0 is userName
				if(loginStage == 0){

          // Copy the buffer to the userName string, increment loginStage
					strncpy(userName, buf, numbytes);
					loginStage++;

          // Loginstage of 1 is password
				}else if(loginStage == 1){

          // Copy buffer to the password string, set logging in to 0
					strncpy(password, buf, numbytes);
					loggingIn = 0;
				}

        // If the user closed connection prematurely, return 0 to free thread
			}else{
        return 0;
      }
		}
	}

// Set counter to 0
int counter = 0;

// Open authentication text and compare the username and passwords
	file = fopen("Authentication.txt", "r"); /* should check the result */

    char line[256];
    char *compString = (char *)malloc((strlen(userName)+strlen(password)+2) * sizeof(char));

    // combine username and password into one string
    strcpy(compString, userName);
		strcat(compString, ".");
    strcat(compString, password);


    // Go through each line and compare login details
    while (fgets(line, sizeof(line), file)) {

    	char fileUserName[20], filePassword[20];
    	sscanf(line, "%s %s", fileUserName, filePassword);

      // Combine the username and password in the file into one string
    	combined = (char *)malloc((strlen(fileUserName)+strlen(filePassword))* sizeof(char));
    	strcpy(combined, fileUserName);
			strcat(combined, ".");
    	strcat(combined, filePassword);

      // If the strings match, return the index of the user it matches
    	if(strcmp(combined, compString) == 0){

    		fclose(file);
        file = NULL;
        free(combined);
        free(compString);
    		return counter;
    	}

      // Free the memory and try again
			free(combined);
      counter++;

    }
    // Free memory, close file, user did not log in
		free(compString);
    fclose(file);
    file = NULL;
    return 0;
}


// Handles closng the server gracefully
void closeHandler(int sig){

  // Get rid of the highscore mutexes
	pthread_mutex_destroy(&Read);
	pthread_mutex_destroy(&Write);

  // Close all threads
  for (int i = 0; i < NUM_THREADS; i++) {
    pthread_cancel(p_threads[i]);
  }

  // Close file
  if (file != NULL){
    fclose(file);
  }

	printf("\nClosing server\n");
	exit(0);


}
